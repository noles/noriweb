<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * DeviceRepository.
 *
 */
class DevicesRepository extends EntityRepository
{
    public function findLocations($region = null, $city = null, $building = null, $room = null, $orderBy = null, $limit = null, $offset = null)
    {
        $qb = $this->createQueryBuilder('d');
        $qb->select('d.location');
        $qb->andWhere($qb->expr()->orX(
            $qb->expr()->eq("d.location", ":location"),
            $qb->expr()->like("d.location", ":locationwildcard")
        ));

        $locationFilter = '%;%';
        $groupBySql = ' group by substring_index(d0_.location,\';\',1)';

        if ($region !== null) {
            $locationFilter = sprintf('%s', $region);
            $groupBySql = ' group by substring_index(substring_index(d0_.location,\';\',2),\';\',-1)';
        }
        if ($city !== null) {
            $locationFilter .= sprintf(';%s', $city);
            $groupBySql = ' group by substring_index(substring_index(d0_.location,\';\',3),\';\',-1)';
        }
        if ($building !== null) {
            $locationFilter .= sprintf(';%s', $building);
            $groupBySql = ' group by substring_index(substring_index(d0_.location,\';\',4),\';\',-1)';
        }
        if ($room !== null) {
            $locationFilter .= sprintf(';%s', $room);
            $groupBySql = ' group by substring_index(substring_index(d0_.location,\';\',5),\';\',-1)';
        }

        $sql = $qb->getQuery()->getSql();
        $sql .= $groupBySql;

        if ($limit !== null) {
            $sql .= sprintf(' LIMIT %s', $limit);
        }

        if ($offset !== null) {
            $sql .= sprintf(' OFFSET %s', $offset);
        }

        if ($orderBy !== null) {
            $order = [];
            foreach ($orderBy as $key => $order) {
                $order[] = sprintf('%s %s', $key, $order);
            }
            $sql .= sprintf(' ORDER BY %s', join(', ', $order));
        }

        $connection = $this->getEntityManager()->getConnection()->prepare($sql);
        $connection->bindParam(1, sprintf('%s', $locationFilter));
        $connection->bindParam(2, sprintf('%s;%%', $locationFilter));
        $connection->execute();
        $devices = $connection->fetchAll(\PDO::FETCH_ASSOC);

        $locations = [];
        foreach ($devices as $device) {
            $locations[] = $device['location_0'];
        }

        return $locations;
    }

    public function findByLocation($region = null, $city = null, $building = null, $room = null, $orderBy = null, $limit = null, $offset = null) {
        $qb = $this->createQueryBuilder('d');
        $qb->select('d.device');
        $qb->andWhere($qb->expr()->orX(
            $qb->expr()->eq("d.location", ":location"),
            $qb->expr()->like("d.location", ":locationwildcard")
        ));

        $locationFilter = '%;%';
        if ($region !== null) {
            $locationFilter = sprintf('%s', $region);
        }
        if ($city !== null) {
            $locationFilter .= sprintf(';%s', $city);
        }
        if ($building !== null) {
            $locationFilter .= sprintf(';%s', $building);
        }
        if ($room !== null) {
            $locationFilter .= sprintf(';%s', $room);
        }

        $qb->setParameter('location', sprintf('%s', $locationFilter));
        $qb->setParameter('locationwildcard', sprintf('%s;%%', $locationFilter));

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }
        if ($offset !== null) {
            $qb->setFirstResult($offset);
        }
        if ($orderBy !== null) {
            foreach ($orderBy as $key => $order) {
                $qb->addOrderBy($key, $order);
            }
        }

        return $qb->getQuery()->getResult();
    }
}
