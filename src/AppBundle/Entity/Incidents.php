<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Incidents
 *
 * @ORM\Table(name="incidents", indexes={@ORM\Index(name="id", columns={"id"}), @ORM\Index(name="name", columns={"name"}), @ORM\Index(name="device", columns={"device"})})
 * @ORM\Entity
 */
class Incidents
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="level", type="boolean", nullable=true)
     */
    private $level = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=true)
     */
    private $name = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="deps", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $deps = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="startinc", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $startinc = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="endinc", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $endinc = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="usrname", type="string", length=32, nullable=true)
     */
    private $usrname = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="time", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $time = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="grp", type="boolean", nullable=true)
     */
    private $grp = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="device", type="string", length=64, nullable=true)
     */
    private $device = '';


}
