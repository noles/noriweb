<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Links
 *
 * @ORM\Table(name="links", indexes={@ORM\Index(name="id", columns={"id"}), @ORM\Index(name="device", columns={"device"}), @ORM\Index(name="ifname", columns={"ifname"}), @ORM\Index(name="neighbor", columns={"neighbor"}), @ORM\Index(name="nbrifname", columns={"nbrifname"})})
 * @ORM\Entity
 */
class Links
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="device", type="string", length=64, nullable=false)
     */
    private $device;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ifname", type="string", length=32, nullable=true)
     */
    private $ifname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="neighbor", type="string", length=64, nullable=false)
     */
    private $neighbor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nbrifname", type="string", length=32, nullable=true)
     */
    private $nbrifname = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="bandwidth", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $bandwidth = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="linktype", type="string", length=4, nullable=true, options={"fixed"=true})
     */
    private $linktype = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="linkdesc", type="string", length=255, nullable=true)
     */
    private $linkdesc = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="nbrduplex", type="string", length=2, nullable=true, options={"fixed"=true})
     */
    private $nbrduplex = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbrvlanid", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $nbrvlanid = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="time", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $time = '0';


}
