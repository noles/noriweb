<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Locations
 *
 * @ORM\Table(name="locations", indexes={@ORM\Index(name="region", columns={"region"})})
 * @ORM\Entity
 */
class Locations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=32, nullable=false)
     */
    private $region;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city", type="string", length=32, nullable=true)
     */
    private $city = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="building", type="string", length=32, nullable=true)
     */
    private $building = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="x", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $x = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="y", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $y = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="ns", type="integer", nullable=true)
     */
    private $ns = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="ew", type="integer", nullable=true)
     */
    private $ew = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="locdesc", type="string", length=255, nullable=true)
     */
    private $locdesc = '';


}
