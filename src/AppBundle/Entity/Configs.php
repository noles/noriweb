<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Configs
 *
 * @ORM\Table(name="configs", uniqueConstraints={@ORM\UniqueConstraint(name="device", columns={"device"})}, indexes={@ORM\Index(name="device_2", columns={"device"})})
 * @ORM\Entity
 */
class Configs
{
    /**
     * @var string
     *
     * @ORM\Column(name="device", type="string", length=64, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $device;

    /**
     * @var string|null
     *
     * @ORM\Column(name="config", type="text", length=16777215, nullable=true)
     */
    private $config;

    /**
     * @var string|null
     *
     * @ORM\Column(name="changes", type="text", length=16777215, nullable=true)
     */
    private $changes;

    /**
     * @var int|null
     *
     * @ORM\Column(name="time", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $time = '0';


}
