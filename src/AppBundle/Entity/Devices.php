<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Devices
 *
 * @ORM\Table(name="devices", uniqueConstraints={@ORM\UniqueConstraint(name="device", columns={"device"})}, indexes={@ORM\Index(name="device_2", columns={"device"}), @ORM\Index(name="location", columns={"location"}), @ORM\Index(name="contact", columns={"contact"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DevicesRepository")
 *
 */
class Devices
{
    /**
     * @var string
     *
     * @ORM\Column(name="device", type="string", length=64, nullable=false)
     * @ORM\Id
     */
    private $device;

    /**
     * @var int|null
     *
     * @ORM\Column(name="devip", type="integer", nullable=true, options={"unsigned"=true})
     * @JMS\AccessType("public_method")
     * @JMS\Type("string")
     */
    private $ipAddress = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="serial", type="string", length=32, nullable=true)
     */
    private $serial = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=32, nullable=true)
     */
    private $type = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="firstdis", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $firstDiscovered = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="lastdis", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $lastDiscovered = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="services", type="boolean", nullable=true)
     */
    private $services = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="devos", type="string", length=16, nullable=true)
     */
    private $operationSystem = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="bootimage", type="string", length=64, nullable=true)
     */
    private $bootImage = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=true)
     */
    private $location = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     */
    private $contact = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="devgroup", type="string", length=32, nullable=true)
     */
    private $group = '';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="devmode", type="boolean", nullable=true)
     */
    private $mode = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="snmpversion", type="boolean", nullable=true)
     */
    private $snmpVersion = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="readcomm", type="string", length=32, nullable=true)
     */
    private $snmpReadCommunity = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="cliport", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $cliPort = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="login", type="string", length=32, nullable=true)
     */
    private $cliLogin = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="icon", type="string", length=16, nullable=true)
     */
    private $icon = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="origip", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $origIpAddress = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="cpu", type="boolean", nullable=true)
     */
    private $cpu = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="memcpu", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $memCpu = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="temp", type="boolean", nullable=true)
     */
    private $temp = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="cusvalue", type="bigint", nullable=true)
     */
    private $cusvalue = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="cuslabel", type="string", length=32, nullable=true)
     */
    private $cuslabel = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="sysobjid", type="string", length=255, nullable=true)
     */
    private $sysobjId = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="writecomm", type="string", length=32, nullable=true)
     */
    private $snmpWriteCommunity = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="devopts", type="string", length=32, nullable=true, options={"fixed"=true})
     */
    private $options = '';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="size", type="boolean", nullable=true)
     */
    private $size = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="stack", type="boolean", nullable=true, options={"default"="1"})
     */
    private $stack = '1';

    /**
     * @var int|null
     *
     * @ORM\Column(name="maxpoe", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $maxPoe = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="totpoe", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $totalPoe = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="cfgchange", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $configChange = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="cfgstatus", type="string", length=2, nullable=true, options={"default"="--","fixed"=true})
     */
    private $configStatus = '--';

    /**
     * @var string|null
     *
     * @ORM\Column(name="vendor", type="string", length=16, nullable=true)
     */
    private $vendor = '';

    /**
     * @var
     *
     * @ORM\OneToOne(targetEntity="Interfaces", mappedBy="device")
     */
    private $interface;

    /**
     * @var
     *
     * @ORM\OneToOne(targetEntity="Modules", mappedBy="device")
     */
    private $module
    ;

    public function getDevice()
    {
        return $this->device;
    }

    public function getName()
    {
        return $this->device;
    }

    public function getIpAddress()
    {
        return long2ip($this->ipAddress);
    }

    public function setIpAddress(int $ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    public function getSerial()
    {
        return $this->serial;
    }

    public function setSerial(string $serial)
    {
        $this->serial = $serial;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    public function getFirstDiscovered()
    {
        return $this->firstDiscovered;
    }

    public function setFirstDiscovered(int $firstDiscovered)
    {
        $this->firstDiscovered = $firstDiscovered;

        return $this;
    }

    public function getLastDiscovered()
    {
        return $this->lastDiscovered;
    }

    public function setLastDiscovered(int $lastDiscovered)
    {
        $this->lastDiscovered = $lastDiscovered;

        return $this;
    }

    public function getServices()
    {
        return $this->services;
    }

    public function setServices(bool $services)
    {
        $this->services = $services;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    public function getOperationSystem()
    {
        return $this->operationSystem;
    }

    public function setOperationSystem(string $operationSystem)
    {
        $this->operationSystem = $operationSystem;

        return $this;
    }

    public function getBootImage()
    {
        return $this->bootImage;
    }

    public function setBootImage(string $bootImage)
    {
        $this->bootImage = $bootImage;

        return $this;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation(string $location)
    {
        $this->location = $location;

        return $this;
    }

    public function getContact()
    {
        return $this->contact;
    }

    public function setContact(string $contact)
    {
        $this->contact = $contact;

        return $this;
    }

    public function getGroup()
    {
        return $this->group;
    }

    public function setGroup(string $group)
    {
        $this->group = $group;

        return $this;
    }

    public function getMode()
    {
        return $this->mode;
    }

    public function setMode(bool $mode)
    {
        $this->mode = $mode;

        return $this;
    }

    public function getSnmpVersion()
    {
        return $this->snmpVersion;
    }

    public function setSnmpVersion(bool $snmpVersion)
    {
        $this->snmpVersion = $snmpVersion;

        return $this;
    }

    public function getSnmpReadCommunity()
    {
        return $this->snmpReadCommunity;
    }

    public function setSnmpReadCommunity(string $snmpReadCommunity)
    {
        $this->snmpReadCommunity = $snmpReadCommunity;

        return $this;
    }

    public function getCliPort()
    {
        return $this->cliPort;
    }

    public function setCliPort(int $cliPort)
    {
        $this->cliPort = $cliPort;

        return $this;
    }

    public function getCliLogin()
    {
        return $this->cliLogin;
    }

    public function setCliLogin(string $cliLogin)
    {
        $this->cliLogin = $cliLogin;

        return $this;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon(string $icon)
    {
        $this->icon = $icon;

        return $this;
    }

    public function getOrigIpAddress()
    {
        return $this->origIpAddress;
    }

    public function setOrigIpAddress(int $origIpAddress)
    {
        $this->origIpAddress = $origIpAddress;

        return $this;
    }

    public function getCpu()
    {
        return $this->cpu;
    }

    public function setCpu(bool $cpu)
    {
        $this->cpu = $cpu;

        return $this;
    }

    public function getMemCpu()
    {
        return $this->memCpu;
    }

    public function setMemCpu(int $memCpu)
    {
        $this->memCpu = $memCpu;

        return $this;
    }

    public function getTemp()
    {
        return $this->temp;
    }

    public function setTemp(bool $temp)
    {
        $this->temp = $temp;

        return $this;
    }

    public function getCusvalue()
    {
        return $this->cusvalue;
    }

    public function setCusvalue(int $cusvalue)
    {
        $this->cusvalue = $cusvalue;

        return $this;
    }

    public function getCuslabel()
    {
        return $this->cuslabel;
    }

    public function setCuslabel(string $cuslabel)
    {
        $this->cuslabel = $cuslabel;

        return $this;
    }

    public function getSysobjId()
    {
        return $this->sysobjId;
    }

    public function setSysobjId(string $sysobjId)
    {
        $this->sysobjId = $sysobjId;

        return $this;
    }

    public function getSnmpWriteCommunity()
    {
        return $this->snmpWriteCommunity;
    }

    public function setSnmpWriteCommunity(string $snmpWriteCommunity)
    {
        $this->snmpWriteCommunity = $snmpWriteCommunity;

        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions(string $options)
    {
        $this->options = $options;

        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize(bool $size)
    {
        $this->size = $size;

        return $this;
    }

    public function getStack()
    {
        return $this->stack;
    }

    public function setStack(bool $stack)
    {
        $this->stack = $stack;

        return $this;
    }

    public function getMaxPoe()
    {
        return $this->maxPoe;
    }

    public function setMaxPoe(int $maxPoe)
    {
        $this->maxPoe = $maxPoe;

        return $this;
    }

    public function getTotalPoe()
    {
        return $this->totalPoe;
    }

    public function setTotalPoe(int $totalPoe)
    {
        $this->totalPoe = $totalPoe;

        return $this;
    }

    public function getConfigChange()
    {
        return $this->configChange;
    }

    public function setConfigChange(int $configChange)
    {
        $this->configChange = $configChange;

        return $this;
    }

    public function getConfigStatus()
    {
        return $this->configStatus;
    }

    public function setConfigStatus(string $configStatus)
    {
        $this->configStatus = $configStatus;

        return $this;
    }

    public function getVendor()
    {
        return $this->vendor;
    }

    public function setVendor(string $vendor)
    {
        $this->vendor = $vendor;

        return $this;
    }

    public function getInterface()
    {
        return $this->interface;
    }

    public function setInterface(interfaces $interface)
    {
        $this->interface = $interface;

        // set (or unset) the owning side of the relation if necessary
        $newDevice = $interface === null ? null : $this;
        if ($newDevice !== $interface->getDevice()) {
            $interface->setDevice($newDevice);
        }

        return $this;
    }


}
