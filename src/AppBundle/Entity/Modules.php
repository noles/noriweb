<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Modules
 *
 * @ORM\Table(name="modules", indexes={@ORM\Index(name="device", columns={"device"}), @ORM\Index(name="serial", columns={"serial"})})
 * @ORM\Entity
 */
class Modules
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Devices", inversedBy="module")
     * @ORM\JoinColumn(name="device", referencedColumnName="device")
     */
    private $device;

    /**
     * @var string|null
     *
     * @ORM\Column(name="slot", type="string", length=64, nullable=true)
     */
    private $slot = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="model", type="string", length=32, nullable=true)
     */
    private $model = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="moddesc", type="string", length=255, nullable=true)
     */
    private $description = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="serial", type="string", length=32, nullable=true)
     */
    private $serial = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="hw", type="string", length=128, nullable=true)
     */
    private $hw = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="fw", type="string", length=128, nullable=true)
     */
    private $fw = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="sw", type="string", length=128, nullable=true)
     */
    private $sw = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="modidx", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $index = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="modclass", type="boolean", nullable=true, options={"default"="1"})
     */
    private $class = '1';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="modloc", type="string", length=255, nullable=true)
     */
    private $modloc = '';


}
