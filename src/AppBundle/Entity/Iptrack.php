<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Iptrack
 *
 * @ORM\Table(name="iptrack", indexes={@ORM\Index(name="mac", columns={"mac"}), @ORM\Index(name="arpdevice", columns={"arpdevice"}), @ORM\Index(name="arpifname", columns={"arpifname"})})
 * @ORM\Entity
 */
class Iptrack
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mac", type="string", length=16, nullable=false)
     */
    private $mac;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ipupdate", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $ipupdate = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="aname", type="string", length=64, nullable=true)
     */
    private $aname = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="nodip", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $nodip = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="arpdevice", type="string", length=64, nullable=true)
     */
    private $arpdevice = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="arpifname", type="string", length=32, nullable=true)
     */
    private $arpifname = '';


}
