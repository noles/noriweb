<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Nodes
 *
 * @ORM\Table(name="nodes", indexes={@ORM\Index(name="mac", columns={"mac"}), @ORM\Index(name="device", columns={"device"}), @ORM\Index(name="ifname", columns={"ifname"}), @ORM\Index(name="vlanid", columns={"vlanid"}), @ORM\Index(name="noduser", columns={"noduser"})})
 * @ORM\Entity
 */
class Nodes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mac", type="string", length=16, nullable=false)
     */
    private $mac;

    /**
     * @var string|null
     *
     * @ORM\Column(name="oui", type="string", length=32, nullable=true)
     */
    private $oui = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="firstseen", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $firstseen = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="lastseen", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $lastseen = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="device", type="string", length=64, nullable=true)
     */
    private $device = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ifname", type="string", length=32, nullable=true)
     */
    private $ifname = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="vlanid", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $vlanid = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="metric", type="string", length=10, nullable=true)
     */
    private $metric = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="ifupdate", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $ifupdate = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="ifchanges", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $ifchanges = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="noduser", type="string", length=32, nullable=true)
     */
    private $noduser = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="nodesc", type="string", length=255, nullable=true)
     */
    private $nodesc = '';


}
