<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Nodnd
 *
 * @ORM\Table(name="nodnd", indexes={@ORM\Index(name="mac", columns={"mac"}), @ORM\Index(name="nodip6", columns={"nodip6"}), @ORM\Index(name="nddevice", columns={"nddevice"}), @ORM\Index(name="ndifname", columns={"ndifname"})})
 * @ORM\Entity
 */
class Nodnd
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mac", type="string", length=16, nullable=true)
     */
    private $mac = '';

    /**
     * @var binary|null
     *
     * @ORM\Column(name="nodip6", type="binary", nullable=true)
     */
    private $nodip6 = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="ip6changes", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $ip6changes = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="ip6update", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $ip6update = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="tcp6ports", type="string", length=64, nullable=true)
     */
    private $tcp6ports = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="udp6ports", type="string", length=64, nullable=true)
     */
    private $udp6ports = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="srv6type", type="string", length=255, nullable=true)
     */
    private $srv6type = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="srv6os", type="string", length=64, nullable=true)
     */
    private $srv6os = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="srv6update", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $srv6update = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="nddevice", type="string", length=64, nullable=true)
     */
    private $nddevice = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ndifname", type="string", length=32, nullable=true)
     */
    private $ndifname = '';


}
