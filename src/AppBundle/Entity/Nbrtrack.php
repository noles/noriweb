<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Nbrtrack
 *
 * @ORM\Table(name="nbrtrack", indexes={@ORM\Index(name="device", columns={"device"}), @ORM\Index(name="ifname", columns={"ifname"}), @ORM\Index(name="neighbor", columns={"neighbor"}), @ORM\Index(name="time", columns={"time"})})
 * @ORM\Entity
 */
class Nbrtrack
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="device", type="string", length=64, nullable=true)
     */
    private $device = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ifname", type="string", length=32, nullable=true)
     */
    private $ifname = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="neighbor", type="string", length=64, nullable=true)
     */
    private $neighbor = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="time", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $time = '0';


}
