<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Policies
 *
 * @ORM\Table(name="policies", indexes={@ORM\Index(name="id", columns={"id"}), @ORM\Index(name="status", columns={"status"}), @ORM\Index(name="class", columns={"class"})})
 * @ORM\Entity
 */
class Policies
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="class", type="string", length=4, nullable=true, options={"fixed"=true})
     */
    private $class = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="polopts", type="string", length=8, nullable=true, options={"fixed"=true})
     */
    private $polopts = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="target", type="string", length=64, nullable=true)
     */
    private $target = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="device", type="string", length=64, nullable=true)
     */
    private $device = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=128, nullable=true)
     */
    private $type = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="location", type="string", length=32, nullable=true)
     */
    private $location = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="contact", type="string", length=32, nullable=true)
     */
    private $contact = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="devgroup", type="string", length=64, nullable=true)
     */
    private $devgroup = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ifname", type="string", length=32, nullable=true)
     */
    private $ifname = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="vlan", type="string", length=32, nullable=true)
     */
    private $vlan = '';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="alert", type="boolean", nullable=true)
     */
    private $alert = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="info", type="string", length=255, nullable=true)
     */
    private $info = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="respolicy", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $respolicy = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="usrname", type="string", length=32, nullable=true)
     */
    private $usrname = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="time", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $time = '0';


}
