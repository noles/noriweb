<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Events
 *
 * @ORM\Table(name="events", indexes={@ORM\Index(name="id", columns={"id"}), @ORM\Index(name="source", columns={"source"}), @ORM\Index(name="level", columns={"level"}), @ORM\Index(name="time", columns={"time"}), @ORM\Index(name="class", columns={"class"}), @ORM\Index(name="device", columns={"device"})})
 * @ORM\Entity
 */
class Events
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="level", type="boolean", nullable=true)
     */
    private $level = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="time", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $time = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="source", type="string", length=64, nullable=true)
     */
    private $source = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="info", type="string", length=255, nullable=true)
     */
    private $info = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="class", type="string", length=4, nullable=true, options={"default"="dev","fixed"=true})
     */
    private $class = 'dev';

    /**
     * @var string|null
     *
     * @ORM\Column(name="device", type="string", length=64, nullable=true)
     */
    private $device = '';


}
