<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vlans
 *
 * @ORM\Table(name="vlans", indexes={@ORM\Index(name="vlanid", columns={"vlanid"}), @ORM\Index(name="device", columns={"device"})})
 * @ORM\Entity
 */
class Vlans
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="device", type="string", length=64, nullable=false)
     */
    private $device;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vlanid", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $vlanid = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="vlanname", type="string", length=32, nullable=true)
     */
    private $vlanname = '';


}
