<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inventory
 *
 * @ORM\Table(name="inventory", uniqueConstraints={@ORM\UniqueConstraint(name="serial", columns={"serial"})}, indexes={@ORM\Index(name="serial_2", columns={"serial"})})
 * @ORM\Entity
 */
class Inventory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="state", type="boolean", nullable=true)
     */
    private $state = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=32, nullable=false)
     */
    private $serial;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="assetclass", type="boolean", nullable=true, options={"default"="1"})
     */
    private $assetclass = '1';

    /**
     * @var string|null
     *
     * @ORM\Column(name="assettype", type="string", length=32, nullable=true)
     */
    private $assettype = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="assetnumber", type="string", length=32, nullable=true)
     */
    private $assetnumber = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="assetlocation", type="string", length=255, nullable=true)
     */
    private $assetlocation = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="assetcontact", type="string", length=255, nullable=true)
     */
    private $assetcontact = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="assetupdate", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $assetupdate = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="pursource", type="string", length=32, nullable=true, options={"default"="-"})
     */
    private $pursource = '-';

    /**
     * @var int|null
     *
     * @ORM\Column(name="purcost", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $purcost = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="purnumber", type="string", length=32, nullable=true)
     */
    private $purnumber = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="purtime", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $purtime = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="maintpartner", type="string", length=32, nullable=true)
     */
    private $maintpartner = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="maintsla", type="string", length=32, nullable=true)
     */
    private $maintsla = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="maintdesc", type="string", length=32, nullable=true)
     */
    private $maintdesc = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="maintcost", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $maintcost = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="maintstatus", type="boolean", nullable=true)
     */
    private $maintstatus = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="startmaint", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $startmaint = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="endmaint", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $endmaint = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="endwarranty", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $endwarranty = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="endsupport", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $endsupport = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="endlife", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $endlife = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="usrname", type="string", length=32, nullable=true, options={"default"="-"})
     */
    private $usrname = '-';


}
