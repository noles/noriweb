<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="usrname", columns={"usrname"})})
 * @ORM\Entity
 */
class Users
{
    /**
     * @var string
     *
     * @ORM\Column(name="usrname", type="string", length=32, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $usrname;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=64, nullable=false)
     */
    private $password = '';

    /**
     * @var int
     *
     * @ORM\Column(name="groups", type="smallint", nullable=false, options={"unsigned"=true})
     */
    private $groups = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=64, nullable=true)
     */
    private $email = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=32, nullable=true)
     */
    private $phone = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="time", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $time = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="lastlogin", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $lastlogin = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment = '';

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=16, nullable=false, options={"default"="english"})
     */
    private $language = 'english';

    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="string", length=16, nullable=false, options={"default"="default"})
     */
    private $theme = 'default';

    /**
     * @var bool
     *
     * @ORM\Column(name="volume", type="boolean", nullable=false, options={"default"="60"})
     */
    private $volume = '60';

    /**
     * @var bool
     *
     * @ORM\Column(name="columns", type="boolean", nullable=false, options={"default"="6"})
     */
    private $columns = '6';

    /**
     * @var bool
     *
     * @ORM\Column(name="msglimit", type="boolean", nullable=false, options={"default"="5"})
     */
    private $msglimit = '5';

    /**
     * @var int
     *
     * @ORM\Column(name="miscopts", type="smallint", nullable=false, options={"default"="35","unsigned"=true})
     */
    private $miscopts = '35';

    /**
     * @var string
     *
     * @ORM\Column(name="dateformat", type="string", length=16, nullable=false, options={"default"="j.M y G:i470"})
     */
    private $dateformat = 'j.M y G:i470';

    /**
     * @var string|null
     *
     * @ORM\Column(name="viewdev", type="string", length=255, nullable=true)
     */
    private $viewdev = '';


}
