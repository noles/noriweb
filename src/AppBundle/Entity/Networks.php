<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Networks
 *
 * @ORM\Table(name="networks", indexes={@ORM\Index(name="device", columns={"device"}), @ORM\Index(name="ifname", columns={"ifname"}), @ORM\Index(name="ifip", columns={"ifip"})})
 * @ORM\Entity
 */
class Networks
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="device", type="string", length=64, nullable=false)
     */
    private $device;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ifname", type="string", length=32, nullable=true)
     */
    private $ifname = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="ifip", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $ifip = '0';

    /**
     * @var binary|null
     *
     * @ORM\Column(name="ifip6", type="binary", nullable=true)
     */
    private $ifip6 = '';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="prefix", type="boolean", nullable=true)
     */
    private $prefix = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="vrfname", type="string", length=32, nullable=true)
     */
    private $vrfname = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="vrfrd", type="string", length=16, nullable=true)
     */
    private $vrfrd = '';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status = '0';


}
