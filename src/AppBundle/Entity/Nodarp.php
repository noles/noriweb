<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Nodarp
 *
 * @ORM\Table(name="nodarp", indexes={@ORM\Index(name="mac", columns={"mac"}), @ORM\Index(name="nodip", columns={"nodip"}), @ORM\Index(name="arpdevice", columns={"arpdevice"}), @ORM\Index(name="arpifname", columns={"arpifname"})})
 * @ORM\Entity
 */
class Nodarp
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mac", type="string", length=16, nullable=true)
     */
    private $mac = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="nodip", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $nodip = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="ipchanges", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $ipchanges = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="ipupdate", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $ipupdate = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="tcpports", type="string", length=64, nullable=true)
     */
    private $tcpports = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="udpports", type="string", length=64, nullable=true)
     */
    private $udpports = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="srvtype", type="string", length=255, nullable=true)
     */
    private $srvtype = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="srvos", type="string", length=64, nullable=true)
     */
    private $srvos = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="srvupdate", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $srvupdate = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="arpdevice", type="string", length=64, nullable=true)
     */
    private $arpdevice = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="arpifname", type="string", length=32, nullable=true)
     */
    private $arpifname = '';


}
