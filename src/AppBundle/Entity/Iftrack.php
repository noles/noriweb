<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Iftrack
 *
 * @ORM\Table(name="iftrack", indexes={@ORM\Index(name="mac", columns={"mac"}), @ORM\Index(name="device", columns={"device"}), @ORM\Index(name="vlanid", columns={"vlanid"})})
 * @ORM\Entity
 */
class Iftrack
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mac", type="string", length=16, nullable=false)
     */
    private $mac;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ifupdate", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $ifupdate = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="device", type="string", length=64, nullable=true)
     */
    private $device = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ifname", type="string", length=32, nullable=true)
     */
    private $ifname = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="vlanid", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $vlanid = '0';


}
