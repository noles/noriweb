<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Interfaces
 *
 * @ORM\Table(name="interfaces", indexes={@ORM\Index(name="device", columns={"device"}), @ORM\Index(name="ifname", columns={"ifname"}), @ORM\Index(name="ifidx", columns={"ifidx"}), @ORM\Index(name="int_devif", columns={"device", "ifname"})})
 * @ORM\Entity
 */
class Interfaces
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Devices", inversedBy="interface")
     * @ORM\JoinColumn(name="device", referencedColumnName="device")
     */
    private $device;

    /**
     * @var string
     *
     * @ORM\Column(name="ifname", type="string", length=32, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="ifidx", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $index;

    /**
     * @var string|null
     *
     * @ORM\Column(name="linktype", type="string", length=4, nullable=true, options={"fixed"=true})
     */
    private $linkType = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="iftype", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $type = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ifmac", type="string", length=12, nullable=true, options={"fixed"=true})
     */
    private $mac = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ifdesc", type="string", length=255, nullable=true)
     */
    private $description = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="alias", type="string", length=64, nullable=true)
     */
    private $alias = '';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="ifstat", type="boolean", nullable=true)
     */
    private $stat = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="speed", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $speed = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="duplex", type="string", length=2, nullable=true, options={"fixed"=true})
     */
    private $duplex = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="pvid", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $pvid = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="inoct", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $inOctets = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="inerr", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $inError = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="outoct", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $outOctets = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="outerr", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $outError = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="dinoct", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $dinoct = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="dinerr", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $dinerr = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="doutoct", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $doutoct = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="douterr", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $douterr = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="indis", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $inDiscards = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="outdis", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $outDiscards = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="dindis", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $dindis = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="doutdis", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $doutdis = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="inbrc", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $inbrc = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="dinbrc", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $dinbrc = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="lastchg", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $lastChange = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="poe", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $poe = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment = '';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="trafalert", type="boolean", nullable=true)
     */
    private $trafalert = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="brcalert", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $brcalert = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="macflood", type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $macFlood = '0';

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function setIndex(int $index)
    {
        $this->index = $index;

        return $this;
    }

    public function getLinkType()
    {
        return $this->linkType;
    }

    public function setLinkType(string $linkType)
    {
        $this->linkType = $linkType;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(int $type)
    {
        $this->type = $type;

        return $this;
    }

    public function getMac()
    {
        return $this->mac;
    }

    public function setMac(string $mac)
    {
        $this->mac = $mac;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function setAlias(string $alias)
    {
        $this->alias = $alias;

        return $this;
    }

    public function getStat()
    {
        return $this->stat;
    }

    public function setStat(bool $stat)
    {
        $this->stat = $stat;

        return $this;
    }

    public function getSpeed()
    {
        return $this->speed;
    }

    public function setSpeed(int $speed)
    {
        $this->speed = $speed;

        return $this;
    }

    public function getDuplex()
    {
        return $this->duplex;
    }

    public function setDuplex(string $duplex)
    {
        $this->duplex = $duplex;

        return $this;
    }

    public function getPvid()
    {
        return $this->pvid;
    }

    public function setPvid(int $pvid)
    {
        $this->pvid = $pvid;

        return $this;
    }

    public function getInOctets()
    {
        return $this->inOctets;
    }

    public function setInOctets(int $inOctets)
    {
        $this->inOctets = $inOctets;

        return $this;
    }

    public function getInError()
    {
        return $this->inError;
    }

    public function setInError(int $inError)
    {
        $this->inError = $inError;

        return $this;
    }

    public function getOutOctets()
    {
        return $this->outOctets;
    }

    public function setOutOctets(int $outOctets)
    {
        $this->outOctets = $outOctets;

        return $this;
    }

    public function getOutError()
    {
        return $this->outError;
    }

    public function setOutError(int $outError)
    {
        $this->outError = $outError;

        return $this;
    }

    public function getDinoct()
    {
        return $this->dinoct;
    }

    public function setDinoct(int $dinoct)
    {
        $this->dinoct = $dinoct;

        return $this;
    }

    public function getDinerr()
    {
        return $this->dinerr;
    }

    public function setDinerr(int $dinerr)
    {
        $this->dinerr = $dinerr;

        return $this;
    }

    public function getDoutoct()
    {
        return $this->doutoct;
    }

    public function setDoutoct(int $doutoct)
    {
        $this->doutoct = $doutoct;

        return $this;
    }

    public function getDouterr()
    {
        return $this->douterr;
    }

    public function setDouterr(int $douterr)
    {
        $this->douterr = $douterr;

        return $this;
    }

    public function getInDiscards()
    {
        return $this->inDiscards;
    }

    public function setInDiscards(int $inDiscards)
    {
        $this->inDiscards = $inDiscards;

        return $this;
    }

    public function getOutDiscards()
    {
        return $this->outDiscards;
    }

    public function setOutDiscards(int $outDiscards)
    {
        $this->outDiscards = $outDiscards;

        return $this;
    }

    public function getDindis()
    {
        return $this->dindis;
    }

    public function setDindis(int $dindis)
    {
        $this->dindis = $dindis;

        return $this;
    }

    public function getDoutdis()
    {
        return $this->doutdis;
    }

    public function setDoutdis(int $doutdis)
    {
        $this->doutdis = $doutdis;

        return $this;
    }

    public function getInbrc()
    {
        return $this->inbrc;
    }

    public function setInbrc(int $inbrc)
    {
        $this->inbrc = $inbrc;

        return $this;
    }

    public function getDinbrc()
    {
        return $this->dinbrc;
    }

    public function setDinbrc(int $dinbrc)
    {
        $this->dinbrc = $dinbrc;

        return $this;
    }

    public function getLastChange()
    {
        return $this->lastChange;
    }

    public function setLastChange(int $lastChange)
    {
        $this->lastChange = $lastChange;

        return $this;
    }

    public function getPoe()
    {
        return $this->poe;
    }

    public function setPoe(int $poe)
    {
        $this->poe = $poe;

        return $this;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function setComment(string $comment)
    {
        $this->comment = $comment;

        return $this;
    }

    public function getTrafalert()
    {
        return $this->trafalert;
    }

    public function setTrafalert(bool $trafalert)
    {
        $this->trafalert = $trafalert;

        return $this;
    }

    public function getBrcalert()
    {
        return $this->brcalert;
    }

    public function setBrcalert(int $brcalert)
    {
        $this->brcalert = $brcalert;

        return $this;
    }

    public function getMacFlood()
    {
        return $this->macFlood;
    }

    public function setMacFlood(int $macFlood)
    {
        $this->macFlood = $macFlood;

        return $this;
    }

    public function getDevice()
    {
        return $this->device;
    }

    public function setDevice(Devices $device)
    {
        $this->device = $device;

        return $this;
    }


}
