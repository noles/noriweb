<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Monimap
 *
 * @ORM\Table(name="monimap", indexes={@ORM\Index(name="id", columns={"id"}), @ORM\Index(name="usrname", columns={"usrname"})})
 * @ORM\Entity
 */
class Monimap
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=32, nullable=true)
     */
    private $title = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="filter", type="string", length=255, nullable=true)
     */
    private $filter = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="args", type="string", length=255, nullable=true)
     */
    private $args = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="mapopts", type="string", length=8, nullable=true, options={"fixed"=true})
     */
    private $mapopts = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="usrname", type="string", length=32, nullable=true)
     */
    private $usrname = '';


}
