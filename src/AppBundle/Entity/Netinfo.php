<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Netinfo
 *
 * @ORM\Table(name="netinfo", indexes={@ORM\Index(name="netip", columns={"netip"}), @ORM\Index(name="country", columns={"country"})})
 * @ORM\Entity
 */
class Netinfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="netip", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $netip = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="netdomain", type="string", length=64, nullable=true)
     */
    private $netdomain = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="orgname", type="string", length=32, nullable=true)
     */
    private $orgname = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="orgdomain", type="string", length=32, nullable=true)
     */
    private $orgdomain = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=16, nullable=true)
     */
    private $phone = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="address", type="string", length=64, nullable=true)
     */
    private $address = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="country", type="string", length=2, nullable=true, options={"fixed"=true})
     */
    private $country = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=64, nullable=true)
     */
    private $description = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="origin", type="string", length=32, nullable=true)
     */
    private $origin = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="icon", type="string", length=16, nullable=true)
     */
    private $icon = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="time", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $time = '0';


}
