<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Devices;
use AppBundle\Form\RestDiscoverType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;

/**
 */
class LocationController extends Controller
{
    /**
     * Get the list of locations.
     *
     * @param ParamFetcher $paramFetcher
     * @param string       $page         integer with the page number (requires param_fetcher_listener: force)
     *
     * @return array data
     *
     * @Get("/locations", name="get_regions", options={ "method_prefix" = false })
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     */
    public function getRegionsAction(ParamFetcherInterface $paramFetcher)
    {
        $page = $paramFetcher->get('page');

        $limit = 100;
        $offset = ($page - 1) * $limit;

        $locations = $this->getDoctrine()
            ->getRepository(Devices::class)
            ->findLocations(null, null, null, null, null, $limit, $offset);

        $regions = [];
        foreach ($locations as $location) {
            $regions[] = preg_split('/;/', $location)[0];
        }

        $view = View::create()
            ->setData(array('regions' => $regions));
        return $this->getViewHandler()->handle($view);
    }

    /**
     * Get the list of cities.
     *
     * @param ParamFetcher $paramFetcher
     * @param string       $page         integer with the page number (requires param_fetcher_listener: force)
     *
     * @return array data
     *
     * @Get("/locations/{region}", name="get_cities", options={ "method_prefix" = false })
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     */
    public function getCitiesAction($region, ParamFetcherInterface $paramFetcher)
    {
        $page = $paramFetcher->get('page');

        $limit = 100;
        $offset = ($page - 1) * $limit;

        $locations = $this->getDoctrine()
            ->getRepository(Devices::class)
            ->findLocations($region, null, null, null, null, $limit, $offset);

        $cities = [];
        foreach ($locations as $location) {
            $cities[] = preg_split('/;/', $location)[1];
        }

        $view = View::create()
            ->setData(array('cities' => $cities));
        return $this->getViewHandler()->handle($view);
    }

    /**
     * Get the list of buildings.
     *
     * @param ParamFetcher $paramFetcher
     * @param string       $page         integer with the page number (requires param_fetcher_listener: force)
     *
     * @return array data
     *
     * @Get("/locations/{region}/{city}", name="get_buildings", options={ "method_prefix" = false })
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     */
    public function getBuildingsAction($region, $city, ParamFetcherInterface $paramFetcher)
    {
        $page = $paramFetcher->get('page');

        $limit = 100;
        $offset = ($page - 1) * $limit;

        $locations = $this->getDoctrine()
            ->getRepository(Devices::class)
            ->findLocations($region, $city, null, null, null, $limit, $offset);

        $buildings = [];
        foreach ($locations as $location) {
            $buildings[] = preg_split('/;/', $location)[2];
        }

        $view = View::create()
            ->setData(array('buildings' => $buildings));
        return $this->getViewHandler()->handle($view);
    }

    /**
     * Get the list of rooms.
     *
     * @param ParamFetcher $paramFetcher
     * @param string       $page         integer with the page number (requires param_fetcher_listener: force)
     *
     * @return array data
     *
     * @Get("/locations/{region}/{city}/{building}", name="get_rooms", options={ "method_prefix" = false })
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     */
    public function getRoomsAction($region, $city, $building, ParamFetcherInterface $paramFetcher)
    {
        $page = $paramFetcher->get('page');

        $limit = 100;
        $offset = ($page - 1) * $limit;

        $locations = $this->getDoctrine()
            ->getRepository(Devices::class)
            ->findLocations($region, $city, $building, null, null, $limit, $offset);

        $rooms = [];
        foreach ($locations as $key => $location) {
            $locations[$key] = preg_replace('/;$/', '', $location);
        }
        $locations = array_unique($locations);

        foreach ($locations as $location) {
            $room = null;
            $roomName = '';

            if (isset(preg_split('/;/', $location)[3])) {
                $room = preg_split('/;/', $location)[3];
                $roomName = $room;
            }

            $devices = $this->getDoctrine()
                ->getRepository(Devices::class)
                ->findByLocation($region, $city, $building, $room, null, $limit, $offset);

            $rooms[] = [
                'name' => $roomName,
                'devices' => $devices,
            ];
        }

        $view = View::create()
            ->setData(array('rooms' => $rooms));
        return $this->getViewHandler()->handle($view);
    }

    /**
     * @return \FOS\RestBundle\View\ViewHandler
     */
    private function getViewHandler()
    {
        return $this->container->get('fos_rest.view_handler');
    }
}
