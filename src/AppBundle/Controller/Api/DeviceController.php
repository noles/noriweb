<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Devices;
use AppBundle\Form\RestDiscoverType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;

/**
 */
class DeviceController extends Controller
{
    /**
     * @param Request $request
     * @return View
     *
     * @Post("/discover", name="post_discover", options={ "method_prefix" = false })
     *
     */
    public function postDiscoverAction(Request $request)
    {
        $body = $request->getContent();
        $data = json_decode($body, true);
    
        $form = $this->createForm(RestDiscoverType::class, null, ['csrf_protection' => false]);

        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {

            //Discover
            $response = [];

            $data = $form->getData();
            $nedipath = '/var/nedi';
            $nediSkipOptions = 'tedbwgG';

            if ($data['rediscover'] !== true) {
                $device = $this->getDoctrine()
                    ->getRepository(Devices::class)
                    ->findOneBy(['ipAddress' => ip2long($data['ip'])]);
                if ($device) {
                    throw new \Exception('Device with IpAddress already discovered');
                }
                $nediSkipOptions .= 'vmAFpjiuaoWPLM';
            }

            exec("$nedipath/nedi.pl -a ". $data['ip'] ." -S".$nediSkipOptions." -v -x /opt/nediDiscover/start.sh 2>&1", $output, $retval);
            if ($retval !== 0) {
                return new JsonResponse('Discovery failed');
            }

            foreach ($output as $line) {
                if (preg_match('/^IDNT:.*TYPE=(.*)/', $line, $match)) {
                    $response['type'] = $match[1];
                }
            }
    
            #throw new \Exception(print_r($output, true));

            return new JsonResponse($response);
        }

        $view = View::create()
            ->setStatusCode(Response::HTTP_BAD_REQUEST)
            ->setData(array(
                'form' => $form,
            ));

        return $this->getViewHandler()->handle($view);
    }

    /**
     * Get the list of devices.
     *
     * @param ParamFetcher $paramFetcher
     * @param string       $page         integer with the page number (requires param_fetcher_listener: force)
     *
     * @return array data
     *
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     */
    public function getDevicesAction(ParamFetcherInterface $paramFetcher)
    {
        $page = $paramFetcher->get('page');

        $limit = 100;
        $offset = ($page - 1) * $limit;

        $devices = $this->getDoctrine()
            ->getRepository(Devices::class)
            ->findBy([], null, $limit, $offset);

        $view = View::create()
            ->setData(array('devices' => $devices));
        return $this->getViewHandler()->handle($view);
    }

    /**
     * Get the device.
     *
     * @param string $name
     *
     * @return array data
     *
     */
    public function getDeviceAction($name)
    {
        $device = $this->getDoctrine()
            ->getRepository(Devices::class)
            ->find($name);

        if (!$device) {
            throw $this->createNotFoundException(
                'No device found for name '.$name
            );
        }

        $view = View::create()
            ->setData(array('device' => $device));
        return $this->getViewHandler()->handle($view);
    }

    /**
     * Delete the device.
     *
     * @param string $name
     *
     * @return View
     *
     */
    public function deleteDeviceAction($name)
    {
        $device = $this->getDoctrine()
            ->getRepository(Devices::class)
            ->find($name);

        if (!$device) {
            throw $this->createNotFoundException(
                'No device found for name '.$name
            );
        }

        $this->getDoctrine()
            ->getManager()
            ->remove($device);

        $this->getDoctrine()
            ->getManager()
            ->flush();

        $view = View::create('',Response::HTTP_NO_CONTENT);
        return $this->getViewHandler()->handle($view);
    }

    /**
     * @return \FOS\RestBundle\View\ViewHandler
     */
    private function getViewHandler()
    {
        return $this->container->get('fos_rest.view_handler');
    }
}
