// assets/js/app.js

require('../css/app.css');
require('../css/global.scss');

/**
 * Create a fresh Vue Application instance
 */
import Vue from 'vue'

new Vue({
    el: '#app',
});

/**
 * Create bootstrap with jquery
 */
const $ = require('jquery');
// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
require('bootstrap');

// End
console.log('Hello Webpack Encore');
