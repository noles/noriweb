NORIWeb
==========

WebInterface for NORI

## Install

### Permissions
```bash
HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1` && \
   sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var/cache var/logs var/sessions && \
   sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var/cache var/logs var/sessions
```

### Assets
compile assets once
```bash
yarn encore dev
```

or, recompile assets automatically when files change
```bash
yarn encore dev --watch
```

on deploy, create a production build
```bash
yarn encore production
```
